package com.lps.controllers

import com.lps.models.DB
import com.lps.models.Message
import org.springframework.web.bind.annotation.*
import java.util.ArrayList

/**
 * Created by roxyh on 09-Jan-18.
 */
@RestController
@RequestMapping("/api/board")
class BoardController {

    @GetMapping("/") // devuelve todos los mensajes
    fun getAll(): ArrayList<Message> {

        val query = DB.connection.prepareStatement("SELECT * FROM messages ORDER BY date DESC LIMIT 20")

        val list = ArrayList<Message>()

        val cursor = query.executeQuery()

        while (cursor.next())
            list.add(Message(cursor))

        return list

    }

    @GetMapping("/news/{id}") // devuelve los mensajes a partir de uno seleccionado
    fun getLast(@PathVariable id:Long): ArrayList<Message> {

        val query = DB.connection.prepareStatement("SELECT * FROM messages WHERE id > ? ORDER BY date DESC LIMIT 20")

        query.setLong(1,id)

        val list = ArrayList<Message>()
        val cursor = query.executeQuery()

        while (cursor.next())
            list.add(Message(cursor))

        return list

    }

    @GetMapping("/delete/{id}") // elimina un mensaje
    fun deleteOne(@PathVariable id:Long): ArrayList<Message> {

        val query = DB.connection.prepareStatement("DELETE FROM messages WHERE id = ?")
        query.setLong(1,id)

        query.execute()

        return getAll()
    }

    @PostMapping("/new") // crea un nuevo mensaje
    fun newMessage(@RequestBody message: Message): Message {

        message.save()

        return message
    }

    @GetMapping("/clean") // borra todos los mensajes
    fun cleanBoard(): List<Message> {

        val query = DB.connection.prepareStatement("DELETE FROM messages")

        query.execute()

        return emptyList<Message>()

    }


}