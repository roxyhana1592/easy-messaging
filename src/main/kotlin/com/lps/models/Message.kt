package com.lps.models

import java.sql.ResultSet
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by roxyh on 09-Jan-18.
 */
class Message () {

    var id: Long = 0L
    var author: String = ""
    var message: String = ""
    var date: String = ""

    constructor(cursor:ResultSet):this(){
        id = cursor.getLong("id")
        author = cursor.getString("author")
        message = cursor.getString("message")
        date = cursor.getString("date")
    }

    fun save(){

        val now = Date()
        date = format.format(now)
        val execute = DB.connection.prepareStatement(
                "INSERT INTO messages (author,message,date) VALUES (?,?,?)"
        )
        execute.setString(1,author)
        execute.setString(2,message)
        execute.setString(3,date)

        execute.execute()

        val keys = execute.generatedKeys
        keys.next()
        id = keys.getLong(1)
    }

    companion object {
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
    }



}