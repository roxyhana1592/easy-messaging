package com.lps.models

import java.sql.Connection
import java.sql.DriverManager

/**
 * Created by roxyh on 09-Jan-18.
 */

object DB {

    val connection : Connection
    private val FILE : String = "database.db";

   init {
       connection = DriverManager.getConnection("jdbc:sqlite:" + FILE);
       createTables()
   }




    private fun createTables() {

        connection.createStatement().execute("CREATE TABLE IF NOT EXISTS messages ( id INTEGER PRIMARY KEY AUTOINCREMENT, message TEXT, author TEXT, date TEXT)")

    }
}
